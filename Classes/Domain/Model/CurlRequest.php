<?php

namespace BolApi\Domain\Model;


class CurlRequest
{
    private $authorizationType = '';
    private $method = '';
    private $accept = '';
    private $url = '';
    private $fields = '';

    private $availableAuthorizationType = ['Basic', 'Bearer'];
    private $availableMethods = ['POST', 'GET'];
    private $availableAccepts = ['application/vnd.retailer.v5+json', 'application/json'];

    /**
     * CurlRequest constructor.
     * @param string $authorizationType
     * @param string $method
     * @param string $accept
     * @param string $url
     * @param string $fields
     */
    public function __construct(string $authorizationType, string $method, string $accept, string $url, string $fields = '')
    {
        if(in_array($authorizationType, $this->availableAuthorizationType))
            $this->authorizationType = $authorizationType;

        if(in_array($method, $this->availableMethods))
            $this->method = $method;

        if(in_array($accept, $this->availableAccepts))
            $this->accept = $accept;

        $this->url = $url;
        $this->fields = $fields;
    }

    /**
     * @return string
     */
    public function getAuthorizationType()
    {
        return $this->authorizationType;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getAccept()
    {
        return $this->accept;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getFields(): string
    {
        return $this->fields;
    }
}