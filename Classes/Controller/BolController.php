<?php
declare(strict_types=1);

namespace BolApi\Controller;

use BolApi\Domain\Model\CurlRequest;

class BolController extends CurlController
{

    /**
     * @param $page
     * @return array
     */
    public function getOpenOrders($page): array
    {
        $openOrdersRequest = new CurlRequest('Bearer', 'GET', 'application/vnd.retailer.v5+json', 'https://api.bol.com/retailer/orders?page=' . $page);
        return $this->request($openOrdersRequest, $this->bearerToken);
    }

    /**
     * @param array $openOrders
     * @return array
     */
    public function handleOpenOrders(array $openOrders): array
    {
        $unhandledOrders = [];
        foreach ($openOrders as $order) {
            $unhandledOrders[] = $this->getSingleOrder($order['orderId']);
        }

        return $unhandledOrders;
    }

    /**
     * @param string $orderId
     * @return array
     */
    private function getSingleOrder(string $orderId): array
    {
        $singleOrderRequest = new CurlRequest('Bearer', 'GET', 'application/vnd.retailer.v5+json', 'https://api.bol.com/retailer/orders/' . $orderId);
        return $this->request($singleOrderRequest, $this->bearerToken);
    }
}