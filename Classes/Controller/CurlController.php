<?php
declare(strict_types=1);

namespace BolApi\Controller;

use BolApi\Domain\Model\CurlRequest;
use Exception;

abstract class CurlController
{

    protected $clientId;
    protected $clientSecret;
    protected $bearerToken;

    /**
     * @param string $clientId
     * @param string $clientSecret
     */
    public function __construct(string $clientId, string $clientSecret)
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
    }

    /**
     * @param string $url
     * @return CurlController
     */
    public function getBearerToken(string $url): CurlController
    {
        if(isset($this->bearerToken))
            return $this;

        $authorizationRequest = new CurlRequest('Basic', 'POST', 'application/json', $url, 'grant_type=client_credentials');
        $bearerToken = $this->request($authorizationRequest, base64_encode($this->clientId . ':' . $this->clientSecret));
        $this->bearerToken = $bearerToken['access_token'];

        return $this;
    }

    /**
     * @param CurlRequest $request
     * @param string $authorization
     * @param string $userPassword
     * @return mixed
     */
    protected function request(CurlRequest $request, string $authorization, string $userPassword = '')
    {
        $response = [];
        try {
            $handle = curl_init();

            if ($handle === false) {
                throw new Exception('Failed to initialize curl');
            }

            $headers = [
                'Content-Type: application/json',
                'Accept: ' . $request->getAccept(),
                'Authorization: ' . $request->getAuthorizationType() . ' ' . $authorization
            ];

            if($request->getMethod() === 'POST') {
                curl_setopt($handle, CURLOPT_POST, true);
                if(strlen($request->getFields()) > 0)
                    curl_setopt($handle, CURLOPT_POSTFIELDS, $request->getFields());
            }

            if(strlen($userPassword) > 0) {
                curl_setopt($handle, CURLOPT_USERPWD, $userPassword);
            }

            curl_setopt($handle, CURLOPT_URL, $request->getUrl());
            curl_setopt($handle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($handle, CURLOPT_HEADER, false);

            $response = curl_exec($handle);

            if ($response === false) {
                echo 'dit?';
                throw new Exception(curl_error($handle), curl_errno($handle));
            }

            curl_close($handle);
        } catch(Exception $e) {
            echo 'dit?';
            trigger_error(sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(), $e->getMessage()),
                E_USER_ERROR);
        }

        return json_decode($response, true);
    }
}