<?php
declare(strict_types=1);

namespace BolApi\Controller;

use BolApi\Domain\Model\CurlRequest;


class JorttController extends CurlController
{

    /**
     * @param string $url
     * @return CurlController
     */
    public function getBearerToken(string $url): CurlController
    {
        if(isset($this->bearerToken))
            return $this;

        $authorizationRequest = new CurlRequest('Basic', 'POST', 'application/json', $url, json_encode(["grant_type" => "client_credentials", "scope" => "invoices:write customers:write"]));
        $bearerToken = $this->request($authorizationRequest, base64_encode($this->clientId . ':' . $this->clientSecret));

        $this->bearerToken = $bearerToken['access_token'];

        return $this;
    }

    /**
     * @param array $customerData
     * @return string
     */
    public function createUser(array $customerData): string
    {
        $createUserRequest = new CurlRequest('Bearer', 'POST', 'application/json', 'https://api.jortt.nl/customers', json_encode($customerData));
        return $this->request($createUserRequest, $this->bearerToken)['data']['id'];
    }

    /**
     * @param array $invoiceData
     * @return string|null
     */
    public function createInvoice(array $invoiceData): ?string
    {
        $createInvoiceRequest = new CurlRequest('Bearer', 'POST', 'application/json', 'https://api.jortt.nl/invoices', json_encode($invoiceData));
        $result = $this->request($createInvoiceRequest, $this->bearerToken);

        if(array_key_exists('id', $result['data']))
            return $result['data']['id'];

        return null;
    }
}