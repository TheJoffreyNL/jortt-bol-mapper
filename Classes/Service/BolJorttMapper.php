<?php
declare(strict_types=1);

namespace BolApi\Service;

class BolJorttMapper
{
    /**
     * @param array $order
     * @return array
     */
    public static function mapClient(array $order): array
    {
        $client = $order['shipmentDetails'];
        if(array_key_exists('company', $client)){
            $clientData['customer_name'] = $client['company'];
            $clientData['is_private'] = false;
            $clientData['attn'] = $client['firstName']." ".$client['surname'];
        } else{
            $clientData['is_private'] = true;
            $clientData['customer_name'] = $client['firstName']." ".$client['surname'];
        }

        if(array_key_exists('houseNumberExtension', $client)){
            $clientData['address_street'] = $client['streetName'].' '.$client['houseNumber'].' '.$client['houseNumberExtension'];
        } else{
            $clientData['address_street'] = $client['streetName'].' '.$client['houseNumber'];
        }

        $clientData['additional_information'] = 'BOL.COM '.$order['orderId'];
        $clientData['address_postal_code'] = $client['zipCode'];
        $clientData['address_city'] = $client['city'];
        $clientData['address_country_code'] = $client['countryCode'];

        return $clientData;
    }

    /**
     * @param array $order
     * @param string $customerId
     * @return array
     */
    public static function mapInvoice(array $order, string $customerId): array
    {
        $invoices = $order['orderItems'];
        $invoiceData['customer_id'] = $customerId;
        $invoiceData['net_amounts'] = true;

        for($i = 0; $i < count($invoices); $i++) {
            $invoiceData['line_items'][$i]['vat'] = '21';
            $invoiceData['line_items'][$i]['units'] = $invoices[$i]['quantity'];
            $invoiceData['line_items'][$i]['amount_per_unit']['value'] = $invoices[$i]['unitPrice'];
            $invoiceData['line_items'][$i]['amount_per_unit']['currency'] = 'EUR';
            $invoiceData['line_items'][$i]['description'] = $invoices[$i]['product']['title'];
        }

        return $invoiceData;
    }
}