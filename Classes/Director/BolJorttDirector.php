<?php
declare(strict_types=1);

namespace BolApi\Director;

use BolApi\Controller\BolController;
use BolApi\Controller\JorttController;
use BolApi\Service\BolJorttMapper;
use Exception;

class BolJorttDirector
{

    /**
     * @param BolController $bolController
     * @param JorttController $jorttController
     * @param string $country
     */
    public function importInvoices(BolController $bolController, JorttController $jorttController, string $country)
    {
        $result = $this->getOrders($bolController);
		
		if(!array_key_exists(0, $result)) {
            echo '<p>Er zijn geen openstaande orders gevonden voor ' . $country . '</p>';
            return;
        }
		
        $openOrders = $result[0];

        if(!count($openOrders)) {
            echo '<p>Er zijn geen openstaande orders gevonden voor ' . $country . '</p>';
            return;
        }

        $openOrders = $bolController->handleOpenOrders($openOrders);

        foreach($openOrders as $openOrder) {
            $jorttCustomer = BolJorttMapper::mapClient($openOrder);

            try {
                $customerId = $this->createCustomer($jorttController, $jorttCustomer);

                $jorttInvoice = BolJorttMapper::mapInvoice($openOrder, $customerId);
                $orderSuccess = $jorttController->createInvoice($jorttInvoice);

                if($orderSuccess) {
                    echo '<p>De order van <b>' . $jorttCustomer['customer_name'] . '</b> (' . $jorttCustomer['address_street'] . '), is succesvol geïmporteerd.</p>';
                } else {
                    echo '<p>Er is iets fout gegaan met de order van <b>' . $jorttCustomer['customer_name'] . '</b> (' . $jorttCustomer['address_street'] . '), neem contact op met Tim. - </p>';
                }
            } catch(Exception $e) {
                trigger_error(sprintf(
                    'Jortt customer/invoice creation failed with error #%d: %s',
                    $e->getCode(), $e->getMessage()),
                    E_USER_ERROR);
            }
        }
    }

    /**
     * @param JorttController $jorttController
     * @param array $customerData
     * @return string
     */
    private function createCustomer(JorttController $jorttController, array $customerData): string
    {
        return $jorttController->getBearerToken('https://app.jortt.nl/oauth-provider/oauth/token')->createUser($customerData);
    }

    /**
     * @param BolController $bolController
     * @param int $page
     * @param array $openOrders
     * @return array
     */
    private function getOrders(BolController $bolController, $page = 1, $openOrders = []): array
    {
        $response = $bolController->getBearerToken('https://login.bol.com/token?grant_type=client_credentials')->getOpenOrders($page);
		if(!array_key_exists('orders', $response))
			return [];
		
		$openOrders[] = $response['orders'];
		
        if(count($openOrders) >= (50 * $page)) {
            $this->getOrders($bolController, ++$page, $openOrders);
        }
        return $openOrders;
    }
}