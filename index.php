<?php

require "vendor/autoload.php";

use BolApi\Controller\BolController;
use BolApi\Controller\JorttController;
use BolApi\Director\BolJorttDirector;

$bolNLController = new BolController('id', 'secret');
$jorttController = new JorttController('id', 'secret');

$bolJorttDirector = new BolJorttDirector();

$bolJorttDirector->importInvoices($bolNLController, $jorttController, 'Nederland');
